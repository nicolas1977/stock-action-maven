package com.programming.ioFiles;

public class Pensee {
    String nom;
    Double taille;
    static final String type = "PENSEE";

    public Pensee(String nom, Double taille) {
        this.nom = nom;
        this.taille = taille;
    }
}
