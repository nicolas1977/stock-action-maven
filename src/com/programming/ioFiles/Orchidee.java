package com.programming.ioFiles;

public class Orchidee {
    String nom;
    Double taille;
    static final String type = "ORCHIDEE";

    public Orchidee(String nom, Double taille) {
        this.nom = nom;
        this.taille = taille;
    }
}
